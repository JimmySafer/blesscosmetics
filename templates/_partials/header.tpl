{* 
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block} *}

{block name='header_nav'}
  <div class="sidenav-overlay"></div>
  <div class="header-nav">
    <a href="#" id="bottoneMenu"><i class="small material-icons">menu</i></a>
    {block name='header_logo'}
      <div class="logo-cont">
        <a class="logo" href="{$urls.base_url}" title="{$shop.name}">
          <img src="{$shop.logo}" alt="{$shop.name}">
        </a>
      </div>
    {/block}
    <div class="nav-info">
      {hook h='displayNav2'}
    </div>
  </div>
{/block}

{block name='header_top'}
  {* <nav class="header-top">
    {hook h='displayTop'}
  </nav> *}

  {hook h='displayNavFullWidth'}

{/block}
