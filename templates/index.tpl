
{extends file='page.tpl'}

    {block name='page_content_container'}
      <section id="content" class="page-home">
        {block name='page_content_top'}{/block}

        {block name='page_content'}

          <div class="home-banner" style="background-image: url('/blesscosmetics/modules/singlebannerhomepage/content/bannerone.jpg');">
            <div class="container">
              <div class="banner-text">
                <span>MAKE UP</span> 
                <span><i>SEMPRE</i></span> 
                <span>PERFETTO.</span> 
                <span><b>100%</b> MADE IN ITALY</span>
              </div>
              <div style="margin-top: 2rem;">
                <a 
                href="{$urls.base_url}2-home" 
                class="mybtn"
                style="
                    border: 2px solid black!important;
                    background: black!Important;
                    padding: 10px 22px!important;
                    color: white!important;">
                      Tutti i prodotti
                </a>
              </div>
            </div>
          </div>
          <div class="top-cat container">
            <a href="{$urls.base_url}matte-lip-tint/22-54-matte-lip-tint.html#/26-colore-aubergine" data-aos="fade-up" data-aos-delay="100">
              <div class="top-cat-img-container">
                <img src="{$thm_dir}themes/blesscosmetics/assets/img/1.jpg" alt="Matte Lip Tint Blesscosmetics">
                <div class="home-cat-actions">
                  <h2>matte</h2>
                  <p>Lip Tint</p>
                  <button class="mybtn">shop now</button>
                </div>
              </div>
            </a>
            <a href="{$urls.base_url}vinyl-lip-tint/23-61-vinyl-lip-tint.html#/32-colore-marilyn" data-aos="fade-up" data-aos-delay="200">
              <div class="top-cat-img-container">
                <img src="{$thm_dir}themes/blesscosmetics/assets/img/2.jpg" alt="Vinyl Lip Tint Blesscosmetics">
                <div class="home-cat-actions">
                  <h2>vinyl</h2>
                  <p>Lip Tint</p>
                  <button class="mybtn">shop now</button>
                </div>
              </div>
            </a>
          </div>

          <div class="container">
            <div class="promo-notification">
              <p><b>Spedizione Gratuita</b> su tutti i prodotti fino al 31 Luglio 2019 senza ordine minimo</p>
            </div>
          </div>

          <div class="kit-section">
            <div class="container" style="text-align: center; align-items:center">
              <div class="img-container" data-aos="fade-up">
                <img style="width: 90%"src="{$thm_dir}themes/blesscosmetics/assets/img/kits.png" alt="Kits Blesscosmetics">
              </div>
              <div class="text-container" data-aos="fade-up" data-aos-delay="100">
                <h3 style="text-align: center;">kits</h3>
                <p>Lorem Ipsum ipsum dolor sit amet consectetur adipisicing elit. Incidunt, asperiores! Veritatis quae beatae eaque quod suscipit vitae? A, odit libero placeat commodi, natus consectetur aut voluptatem qui est quae facilis!</p>
                <a href="{$urls.base_url}20-special-kit" class="mybtn">shop now</a>
              </div>
            </div>
          </div>

          <div class="bottom-cat container">
            <a href="{$urls.base_url}home/25-75-go-for-matte-foundation.html#/38-colore-biscuit" data-aos="fade-up" >
              <div class="bottom-cat-img-container">
                <img src="{$thm_dir}themes/blesscosmetics/assets/img/3.jpg" alt="Fondotinta Blesscosmetics">
                <div class="home-cat-actions">
                  <h2>foundation</h2>
                  <p>Go for Matte</p>
                  <button class="mybtn">shop now</button>
                </div>
              </div>
            </a>
            <a href="{$urls.base_url}home/24-1426-all-over-concealer.html#/36-colore-1" data-aos="fade-up" data-aos-delay="100">
              <div class="bottom-cat-img-container">
                <img src="{$thm_dir}themes/blesscosmetics/assets/img/4.jpg" alt="Correttore Blesscosmetics">
                <div class="home-cat-actions">
                  <h2>concealer</h2>
                  <p>B Perfect</p>
                  <button class="mybtn">shop now</button>
                </div>
              </div>
            </a>
            <a href="{$urls.base_url}mascara/20-all-in-1-mascara-black.html" data-aos="fade-up" data-aos-delay="200">
              <div class="bottom-cat-img-container">
                <img src="{$thm_dir}themes/blesscosmetics/assets/img/5.jpg" alt="Mascara Blesscosmetics">
                <div class="home-cat-actions">
                  <h2>mascara</h2>
                  <p>All-in-One</p>
                  <button class="mybtn">shop now</button>
                </div>
              </div>
            </a>
          </div>

          {block name='hook_home'}
            {$HOOK_HOME nofilter}
          {/block}
        {/block}
      </section>
    {/block}
