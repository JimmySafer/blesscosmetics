
 <div class="container">
  <div>
    <h3 style="text-align: center">Blog</h3>
    <p style="text-align: center">idee di make-up, ispirazioni di look e consigli viso</p>
    <div class="rss_slider">
      {if $rss_links}
          {foreach from=$rss_links item='rss_link'}
          {* GET STRING FROM IMAGE LINK RSS FEED *}
            {$img_url = (string) $rss_link.thumbnail->_url}
            <div>
              <img src="{$img_url}" alt="{$rss_link.title}">
              <div class="rss_slider_text">
                <a href="{$rss_link.link}" title="{$rss_link.title}" target="_blank">{$rss_link.title}</a>
              </div>
            </div>
          {/foreach}
      {else}
        <p>{l s='No RSS feed added' d='Modules.Rssfeed.Shop'}</p>
      {/if}
    </div>
  </div>
 </div>
